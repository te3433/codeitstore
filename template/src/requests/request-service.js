import { ITEMS_PER_PAGE } from "../common/constants.js";
import { data } from "../data/data.js";

export const loadCards = () => {
  return data;
};

export const recalculatePagesNumber = (itemsData) => {
  let pagesArray = [];

  const pagesNumber = itemsData.length / ITEMS_PER_PAGE;
  for (let i = 1; i <= pagesNumber + 1; i++) {
    pagesArray.push(i);
  }
  return pagesArray;
};
export const loadPagesNumber = () => {
  let pagesArray = [];

  const pagesNumber = data.length / ITEMS_PER_PAGE;
  for (let i = 1; i <= pagesNumber + 1; i++) {
    pagesArray.push(i);
  }
  return pagesArray;
};

export const removeItem = (id) => {
  const onlyIds = data.map((obj) => obj.id);
  const index = onlyIds.indexOf(+id);

  if (index > -1) {
    // only splice array when item is found
    data.splice(index, 1); // remove one item only
  }
};

export const loadSearchCards = (searchTerm = "") => {
  const searchedData = data.filter((obj) =>
    obj.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return searchedData;
};
