import { toSinglePaginationButton } from "./pagination-button-view.js";

export const toPaginationView = (allPages = []) => `
<ul class="pagination">
${allPages.map((page) => toSinglePaginationButton(page)).join("\n")}
</ul>
`;
