import { toCardSimple } from "./card-view.js";

export const toOnePageView = (data = []) => `
 
  <div class="row">
  ${data.map((card) => toCardSimple(card)).join("\n")}
</div>
`;
