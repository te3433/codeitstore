import { toCardSimple } from "./card-view.js";

export const toSearchView = (cards, searchTerm) => `
<div class="container">  
  <h1>
  ${
    searchTerm.length > 0
      ? ` ${cards.length} Wish Items found for "${searchTerm}":`
      : "Please, try another search criteria"
  }</h1>
  <br>
  <br>
  <div class="row">
  ${
    cards
      ? cards.map((card) => toCardSimple(card)).join("\n")
      : "<p>No wish items were found...</p>"
  }
  </div>
</div>
</div>
`;
