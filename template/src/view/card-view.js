export const toCardSimple = (item, local = null) => `
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
          <div class="card shopping-card-item mb-5">
            <img
              class="card-img-top"
              src="${item.ImageUrl}"
              alt="Card image cap"
            />
            <button id = ${item.id} class="delete-item-button btn-hide">REMOVE FROM LIST</button>
            <div class="card-body pb-0 d-flex flex-column">
              <div class="logo-line d-flex mx-auto"></div>
              <p class="card-title mt-3 mb-3 d-flex mx-auto">${item.name}</p>
              <p class="card-text d-flex mx-auto">$${item.price}</p>
              <div class="d-flex mx-auto">
                <button id = ${item.id} class="delete-item-button border shopping-button"> <i class="fa-solid fa-trash-can" style="pointer-events:none"></i></button>
                </button>
                <button  class=" border shopping-button">
                    <i class="fa-solid fa-cart-shopping"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
`;
