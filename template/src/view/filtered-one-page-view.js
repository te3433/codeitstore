import { toCardSimple } from "./card-view.js";

export const toFilteredOnePageView = (cards, searchTerm, totalItems) => `

<div class="container">  
  <h1>
  ${
    totalItems > 0
      ? ` ${totalItems} Wish Items found for "${searchTerm}":`
      : "<p>No wish items were found... <p>Please, try another search criteria</p></p>"
  }</h1>
  <br>
  <br>
  <div class="row">
  ${cards && cards.map((card) => toCardSimple(card)).join("\n")}
  </div>
</div>
</div>
`;
