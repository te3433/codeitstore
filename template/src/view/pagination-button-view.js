export const toSinglePaginationButton = (pageNumber) => `
<li class=""><a id =${pageNumber} class="page-link shadow-none" href="#">${pageNumber}</a></li>
`;
