export const HOME = "home";

export const CONTAINER_SELECTOR = "#container";

export const PAGINATION_CONTAINER_SELECTOR = "#pagination-container";

export const SEARCH = "search";

export const FIRST_PAGE = 1;

export const ITEMS_PER_PAGE = 8;
