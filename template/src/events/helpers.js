export const q = (selector) => document.querySelector(selector);

export const qs = (selector) => document.querySelectorAll(selector);

export const setActiveNav = (page) => {
  const navs = qs("a.nav-link");

  Array.from(navs).forEach((element) =>
    element.getAttribute("data-page") === page
      ? element.classList.add("active")
      : element.classList.remove("active")
  );
};
export const enableButton = () => {
  q("button.clear-list-button").disabled = false;
};

export const disableButton = () => {
  q("button.clear-list-button").disabled = true;
};

export const clearInput = () => {
  q("input#form1").value = "";
  disableButton();
};

export const setSubscriptionAlert = () => {
  const errorField = q("#error-field");
  const successField = q("#success-field");
  if (!q("input#form2").value) {
    errorField.classList.add("show-alert");
  } else {
    errorField.classList.remove("show-alert");
    successField.classList.add("show-alert");
    q("input#form2").value = "";
  }
};
