import { CONTAINER_SELECTOR } from "../common/constants.js";
import { q } from "./helpers.js";
import { loadSearchCards } from "../requests/request-service.js";
import { toSearchView } from "../view/search-view.js";
import { renderRecalculatedPagesButtons } from "./navigation-events.js";

export const renderSearchCards = (searchTerm) => {
  const data = loadSearchCards(searchTerm);

  q(CONTAINER_SELECTOR).innerHTML = toSearchView(data, searchTerm);
  return data;
};
