import {
  CONTAINER_SELECTOR,
  FIRST_PAGE,
  HOME,
  ITEMS_PER_PAGE,
  PAGINATION_CONTAINER_SELECTOR,
} from "../common/constants.js";
import {
  loadCards,
  loadPagesNumber,
  recalculatePagesNumber,
} from "../requests/request-service.js";
import { toFilteredOnePageView } from "../view/filtered-one-page-view.js";
import { toOnePageView } from "../view/one-page-vew.js";
import { toPaginationView } from "../view/pagination-view.js";
import { q, setActiveNav } from "./helpers.js";

export const loadPage = async (page = "") => {
  switch (page) {
    case HOME:
      setActiveNav(HOME);
      renderAllPagesButtons();
      return renderOnePageCards(FIRST_PAGE);

    /* if the app supports error logging, use default to log mapping errors */
    default:
      return null;
  }
};

export const renderOnePageCards = (pageNumber) => {
  const data = loadCards();
  const currentPageLastIndex = pageNumber * ITEMS_PER_PAGE;
  let currentPageFirstIndex = currentPageLastIndex - ITEMS_PER_PAGE;
  if (currentPageFirstIndex === -1) {
    currentPageFirstIndex = 0;
  }

  const onePageData = data.slice(currentPageFirstIndex, currentPageLastIndex);
  q(CONTAINER_SELECTOR).innerHTML = toOnePageView(onePageData);
};

export const renderRecalculatedOnePageCards = (
  itemsData,
  pageNumber,
  searchTerm
) => {
  const totalItems = itemsData.length;
  const currentPageLastIndex = pageNumber * ITEMS_PER_PAGE;
  let currentPageFirstIndex = currentPageLastIndex - ITEMS_PER_PAGE;
  if (currentPageFirstIndex === -1) {
    currentPageFirstIndex = 0;
  }

  const onePageData = itemsData.slice(
    currentPageFirstIndex,
    currentPageLastIndex
  );
  console.log(onePageData);
  q(CONTAINER_SELECTOR).innerHTML = toFilteredOnePageView(
    onePageData,
    searchTerm,
    totalItems
  );
};

export const renderAllPagesButtons = () => {
  const pagesNumber = loadPagesNumber();

  q(PAGINATION_CONTAINER_SELECTOR).innerHTML = toPaginationView(pagesNumber);
};

export const renderRecalculatedPagesButtons = (itemsData) => {
  const pagesNumber = recalculatePagesNumber(itemsData);

  q(PAGINATION_CONTAINER_SELECTOR).innerHTML = toPaginationView(pagesNumber);
};
