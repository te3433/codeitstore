import { FIRST_PAGE, HOME, SEARCH } from "./common/constants.js";
import {
  clearInput,
  enableButton,
  q,
  setSubscriptionAlert,
} from "./events/helpers.js";
import {
  loadPage,
  renderAllPagesButtons,
  renderOnePageCards,
  renderRecalculatedOnePageCards,
  renderRecalculatedPagesButtons,
} from "./events/navigation-events.js";
import { renderSearchCards } from "./events/search-events.js";
import { loadSearchCards, removeItem } from "./requests/request-service.js";

document.addEventListener("DOMContentLoaded", async () => {
  let currentPage = FIRST_PAGE;
  let location = HOME;
  let searchTerm = "";
  document.addEventListener("click", async (event) => {
    if (event.target.classList.contains("delete-item-button")) {
      removeItem(event.target.id);
      renderOnePageCards(currentPage);
    }

    if (event.target.classList.contains("clear-list")) {
      clearInput();
      renderOnePageCards(FIRST_PAGE);
      renderAllPagesButtons();
    }
    if (location === HOME && event.target.classList.contains("page-link")) {
      renderOnePageCards(event.target.id);
      currentPage = event.target.id;
    }
    if (location === SEARCH && event.target.classList.contains("page-link")) {
      currentPage = event.target.id;
      const searchedItems = loadSearchCards(searchTerm);
      renderRecalculatedOnePageCards(searchedItems, currentPage, searchTerm);
    }
    if (event.target.classList.contains("email-subscription-btn")) {
      event.preventDefault();
    }
  });

  // search events
  q("input#form1").addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
      location = SEARCH;
      searchTerm = event.target.value;
      const searchedItems = loadSearchCards(event.target.value);

      renderRecalculatedOnePageCards(
        searchedItems,
        FIRST_PAGE,
        event.target.value
      );
      renderRecalculatedPagesButtons(searchedItems);
    }
  });

  q("input#form2").addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
      setSubscriptionAlert();
    }
  });

  q("input#form1").addEventListener("keyup", () => {
    enableButton();
  });

  loadPage(HOME);
});
